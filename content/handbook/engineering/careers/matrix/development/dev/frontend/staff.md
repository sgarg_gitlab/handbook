---
title: "Dev Career Framework: Staff Frontend Engineer"
---

## Dev Staff Frontend Engineer

{{% include "includes/engineering/dev-be-career-matrix-nav.md" %}}

**Dev Staff Frontend Engineers at GitLab are expected to exhibit the following competencies:**

- [Leadership Competencies](#leadership-competencies)
- [Technical Competencies](#technical-competencies)
- [Values Alignment](#values-alignment)

---

### Leadership Competencies

{{% include "includes/engineering/staff-leadership-competency.md" %}}
{{% include "includes/engineering/development-staff-leadership-competency.md" %}}

### Technical Competencies

{{% include "includes/engineering/staff-technical-competency.md" %}}
{{% include "includes/engineering/development-staff-technical-competency.md" %}}
- Expert knowledge of frontend programming languages and GitLab's default frontend frameworks
- Able to identify and drive large architectural changes
- Able to analyze and contribute back improvements to third-party libraries
- Improve engineering standards, tooling, and processes
- Identify, analyze, and fix slow and frontend code architecture and implementation

###  Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-staff-values-competency.md" %}}
