---
title: "Incubation Engineering Department Career Framework: Staff"
---

## Incubation Engineering Department Competencies: Staff

{{% include "includes/engineering/career-matrix-nav.md" %}}

**Staff at GitLab are expected to exhibit the following competencies:**

- [Staff Leadership Competencies](#staff-leadership-competencies)
- [Staff Technical Competencies](#staff-technical-competencies)
- [Staff Values Alignment](#staff-values-alignment)

---

### Staff Leadership Competencies

{{% include "includes/engineering/staff-leadership-competency.md" %}}
  
### Staff Technical Competencies

{{% include "includes/engineering/staff-technical-competency.md" %}}

### Staff Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
