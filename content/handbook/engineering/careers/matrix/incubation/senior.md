---
title: "Incubation Engineering Department Career Framework: Senior"
---

## Incubation Engineering Department Competencies: Senior

{{% include "includes/engineering/career-matrix-nav.md" %}}

**Seniors at GitLab are expected to exhibit the following competencies:**

- [Senior Leadership Competencies](#senior-leadership-competencies)
- [Senior Technical Competencies](#senior-technical-competencies)
- [Senior Values Alignment](#senior-values-alignment)


### Senior Leadership Competencies

{{% include "includes/engineering/senior-leadership-competency.md" %}}
  
### Senior Technical Competencies

{{% include "includes/engineering/senior-technical-competency.md" %}}

### Senior Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
